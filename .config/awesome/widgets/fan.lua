--Fan Mode Watcher For Ideapad or -like computers.
--You can check path of fan mode file from "~/.scripts/fanfilechanger".

local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local build_widget = require("widgets.build_widget")

if os.getenv('IS_LAPTOP') then
  local fan = wibox.widget.textbox('')
  local fan_icon = ''
  local fan_icon_color = beautiful.xcolor7

  awesome.connect_signal("evil::fan", function(value)
    local fan_value = value

    if fan_value == 1 then
      fan_icon = "❄"
      fan_icon_color = beautiful.xcolor6
    elseif fan_value == 2 then
      fan_icon = "⎋"
      fan_icon_color = beautiful.xcolor2
    elseif fan_value == 3 then
        fan_icon = "⚔"
        fan_icon_color = beautiful.xcolor1
    end

    fan:UpdateIcon(fan_icon, fan_icon_color)
  end)

  fan = build_widget:new(fan, fan_icon, fan_icon_color)

  return fan.widget
else
  return wibox.widget.textbox('')
end
