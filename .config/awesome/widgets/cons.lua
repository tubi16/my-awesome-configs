--Conservation Mode Watcher For Ideapad or -like computers.
--You can check path of conservation mode file from "awesome/evil/conservation.lua".

local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local build_widget = require("widgets.build_widget")

if os.getenv('IS_LAPTOP') then
  local cons = wibox.widget.textbox('')
  local cons_icon = ''
  local cons_icon_color = beautiful.xcolor7

  awesome.connect_signal("evil::conservation", function(value)
    local cons_mod = value

    if cons_mod == 1 then
      cons_icon = ""
      cons_icon_color = beautiful.xcolor10
    elseif cons_mod == 0 then
      cons_icon = ""
      cons_icon_color = beautiful.xcolor5
    end

    cons:UpdateIcon(cons_icon, cons_icon_color)
  end)

  cons = build_widget:new(cons, cons_icon, cons_icon_color)

  cons.widget:buttons(awful.util.table.join(
    awful.button({}, 1, function() --left click
      os.execute("echo 1 > /sys/bus/platform/devices/VPC2004:00/conservation_mode")
    end),
    awful.button({}, 3, function() --right click                                         for using this two function, you need change permissions of this file.
      os.execute("echo 0 > /sys/bus/platform/devices/VPC2004:00/conservation_mode")   -- You can create a unit file for this. So you can change it automaticly at start. ^^
    end)
  ))

  return cons.widget
else
  return wibox.widget.textbox('')
end
