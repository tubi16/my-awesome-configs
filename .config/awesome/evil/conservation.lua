local awful = require("awful")
local naughty = require("naughty")

local update_interval = 15

--You must check this please.
local conservation_script = [[
  sh -c "
    cat /sys/bus/platform/devices/VPC2004:00/conservation_mode
  "]]

awful.widget.watch(conservation_script, update_interval, function(widget, stdout)
  local conservation_mode = tonumber(stdout)
  awesome.emit_signal("evil::conservation", conservation_mode)
end)
