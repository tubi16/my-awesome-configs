local awful = require("awful")
local naughty = require("naughty")

local update_interval = 15

--You must check this please.
local fanmode_script = [[
  sh -c "
    cat /home/tubi/.config/awesome/utils/fanvalue
  "]]

awful.widget.watch(fanmode_script, update_interval, function(widget, stdout)
  local fan_mode = tonumber(stdout)
  awesome.emit_signal("evil::fan", fan_mode)
end)
